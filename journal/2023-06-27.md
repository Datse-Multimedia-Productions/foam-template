---
foam-template: 
  filepath: "./journal/2023-06-27.md" 
  type: daily-note
  created: 2023-06-27 16:18:40 (1687907920.402)
---
# 2023-06-27 — 2023-06-27

See [[Bullet Journal Workflow]], [[Daily Note]], and [[2023-03-09 Testing Creators Journal]] for things that should be done here.

* [ ] Bullet Journal Note(s) Created
* [ ] Bullet Journal Note(s) Moved
* [ ] Bullet Journal Migration(s)
* [ ] Creators Journal(s) Created
* [ ] Creators Journal(s) Moved
* [ ] Daily Note Updated
* [ ] Migrate Bullet Journal
