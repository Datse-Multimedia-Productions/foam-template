---
foam-template: 
  filepath: "./journal/creator" 
  type: daily-log
  created: 2023-04-22 16:49:08 (1682207348.46)
  title: 2023-04-22 — Thoughts on Saturday...
---
# 2023-04-22 — Thoughts on Saturday...

Just setting this up for now, want to have this here to do a bit more later.  Got other stuff on our mind right now.

## Brief Update

Not as productive today as I had hoped.  The house has had soap made in it and it's really getting to me, but I did get some stuff done.  

## Learning

I think I have dipped in some stuff, but not in any kind of formal way today, which is expected most days.  

## Coding

Nothing coding wise happened, unless you count working on the Windows 11 machine to get maybe a bit better (not nagging about completing computer setup).  I guess in a way that kind of is.  It's a lot more involved than a lot of the coding I do more regularly.  

## Writing

I did a bit of a post on [Ko-Fi](https://ko-fi.com/post/Journalling-Getting-Stuff-Done-Bulletish-Journal-D1D8KNCO6) about the journalling etcetera that we're doing here.  That's good...  

## Painting/Drawing

I did a bit of playing on the Windows 11 machine in Krita.  It doesn't have a tablet attached so it's more suited for stuff like photo editing.  That's about it.  But that's again, as expected...  

## Sewing

Nothing really.  Some sort of moving the tasks around here to see where they might go.  

## Videos

Nothing.  I did consider looking at the No Man's Sky video, and also a bit about looking at recording another one for talking about the, "don't support JK Rowling" stuff I want to talk about.  Maybe thinking about doing a, "Jigme Spills the Tea" video.  It seemed like a good day to do that, but then by the time I considered setting up, the day turned less good.  

As for the streaming videos, one thing I want to do, is have some way to handle multiple stream recording in `ffmpeg` so that I can record the audio in a way that feels more likely to allow me to fix it, if it doesn't work out.  

## Games 

I have worked a bit on getting more of the "Solo Quest" from the *Runequest Starter Set* into MapTool.  

### Development

Some work on the "Solo Quest" in MapTool.  Most of it has just been getting the content from the PDF to the campaign file.  

### Playing

Just really been doing the Trimps and Legends of Idleon games.  

## Daily Log

* -- New Notes Here --

## Format Info

* basic note
* [ ] task note
* { } Indicators
* [ ] { } Task with Indicators

