---
foam-template: 
  filepath: "./journal/creator" 
  type: daily-log
  created: 2023-03-09 12:01:25 (1678392085.817)
  title: 2023-03-09 — Testing Creator's Journal
---
# 2023-03-09 — Testing Creator's Journal

This is the start of trying to do a Creator's Journal.  The idea came from [Tasha the Witch Bee](https://www.patreon.com/LittleWitchBee/) over on Patreon.  

## Brief Update

This is more relevant when putting together multiple ones, but I want to have this in the template anyway, so the brief update is that we are starting to work with this template as a way to look at what we have setup here.  

## Learning

So, in reading Tasha's latest update, they mentioned learning Portugese, which reminded me that I have been working on learning Korean over on Duolingo.  

I don't really think that they do a very good job.  Though it is possible that it is simply because what they are doing there, is based on getting a person to "be able to use" the language, more so than a person actually *learning* the language.  

I want to have what it would take to be able to work in the language, and when a language is largely limited to a region (like Korean), it is helpful to have some cultural knowledge, rather than just the ability to "say this".  Still, much better than nothing.  

## Coding

No coding, unless you consider the creation of this template...

## Writing

Nothing much to say here.  Not doing writing other than what I have here so far.

## Painting/Drawing

Watching Tasha's video on Patreon where she's doing work on the Mystical Merlins comic (I think).

## Sewing

Not actually sewing, but today I spotted a tin from some Tiger Balm, which will have needles transfered over to, as I use them, rather than trying to put them back in their original packaging.  

I also ordered some fabric for the omutsu cover that I was working on previously, and I may work on figuring out how that will end up working out with the existing pattern.  

## Videos

I have a *No Man's Sky* video I recorded a few days ago, that I continued to do some playing of that.  I should probably work on getting that posted.  

## Games 

Been playing mostly *No Man's Sky* for the last while.  

### Development

A little while ago, I did a bit of looking at stuff in Godot.  I had seen that 4.0 has been released, so I thought I'd see if I had got it updated myself already, and if not, to update it.

A game I had been working on, or more a tool, which is supposed to display a timer I moved to 4.0 and now it actually works.  I thought that I'd had it so that the button would start the timer, but it wasn't.  

Now the timer does start, and it's counting up (I can do count down with a little code change, which is sort of what I want to do with this).  

### Playing

I have been playing *No Man's Sky*.  I had meant to record it, but I picked up the switch a few days ago, to look at the settings, and played a fair amount then, and I've kind of given up with playing it for videos.  

I'm enjoying the game, but there's a fair amount of work to doing it for videos (even without trying to stream it at all), and so far (not looked at latest one yet), I have had too much trouble with them that they aren't turning out.  I did have some setup that I could record the audio as separate tracks and shouldn't have issues (I had been trying to use Zrythm for this, but it kept crashing).  

I do hope to get my audio/video stuff into the development area (so that I have those tools somewhere I have use of them and all of that).

## Daily Log

* -- New Notes Here --
* [ ] Get Audio/Video tools together
  * [ ] Get Multi-track ffmpeg recording setup
  * [ ] Put h265 re-encode script somewhere easy to find
* [ ] Edit and post No Man's Sky video
* [ ] Omutsu Cover pattern
  * [ ] Lining portion
  * [ ] "Wings" portion attachment
  * [ ] Snaps locations
  * [ ] Print and Test 
* [ ] Diaper Trainer setup timers for:
  * [ ] Changes
  * [ ] Wettings
  * [ ] Messings
* [ ] Migrate Daily Log

## Format Info

* basic note
* [ ] task note
* { } Indicators
* [ ] { } Task with Indicators

