---
foam-template: 
  filepath: "./journal/creator" 
  type: daily-log
  created: 2023-04-21 15:48:21 (1682117301.451)
  title: 2023-04-21 — No idea really
---
# 2023-04-21 — No idea really

This is only our second update to this.  I feel that I'm doing this as a way to look at this process.  

## Brief Update

I have been trying to do some stuff, but not really documenting it.  I am feeling a bit better about being able to do some of the creative things.  A lot actually has happened since the previous creator journal.  Which is part of why I haven't been doing this and why I'm feeling better about doing the creating stuff anyway.  

My bank froze my account...  And I have given up on trying to sort that out.  Which I guess is fine.  Mostly.  It has meant that I am feeling more pressed to find ways to handle my money which don't involve that.  

## Learning

I have mostly given up on Duolingo.  I was enjoying it for a while, and will probably go back to that at some point.  

## Coding

I was working on getting VSCodium and Foam replaced by my own set of tools.  I got a little way down that path, but not far enough, and not even wanting to take too serious of a look at it right now.  

## Writing

I have done several posts on [Ko-Fi](https://ko-fi.com/Jigme), and on Writing.com.  I have just started doing some reviews on [Bookwyrm](https://ramblingreaders.org/jigmedatse).

## Painting/Drawing

Watching [Tasha's](https://www.patreon.com/LittleWitchBee/), stream (VOD), with doing some map making, I looked a little bit at doing some of that, and playing a bit with Inkscape.  I didn't find that Inkscape really gave me what I wanted, so I'll probably do that in Krita instead.  

I also tried to do some stuff with creating a layout for a Sudoku app in Krita, but it never got anywhere.  I *did* create a layout in Inkscape, which I've created a little book that I can start working on the creation of puzzles with it.

## Sewing

No sewing has happened, except *maybe* some repair type stuff.  I did receive the fabric I ordered.  Somewhat disapointing, as it really doesn't seem like it will fit my needs.  

## Videos

No update on the videos.  I did record a video (well several videos) for the phone call with the bank.  I'm pretty sure it's terrible, but honestly...  I think if I do create anything, it will be more of a matter of creating something like a vlog thing.  

## Games 

I continued to play a lot of *No Man's Sky*.  I ended up wanting to switch, so I went to *Atelier Ryza 2* and did a bit of that.  

I have started playing (contiuing more), *Divinity II: Original Sin*, which is ...  Enjoyable, but not as enjoyable as would like, as it just kind of ends up being frustrating a lot of the time.  Will probably go back to the other two games which really when they are frustrating, it's because what I had hoped would be a simple thing, ends up being more involved.  Not so much that it goes badly.  

### Development

Nothing much to say.  I did mention above that I went and worked on some stuff with trying to create a replacement for this way of doing things, but I'm not entirely sure that I'm going to end up doing that.  There's a lot involved there.  

### Playing

Been trying to enjoy the outside a bit.  The weather hasn't been the greatest.  It's been kind of cold, and there's still snow on the ground.  Honestly, we keep getting fresh snow.  

## Daily Log

* -- New Notes Here --
* [x] Migrate from Previous Creator's Journal to Bullet Journal

## Format Info

* basic note
* [ ] task note
* { } Indicators
* [ ] { } Task with Indicators

