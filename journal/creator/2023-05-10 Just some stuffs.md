---
foam-template: 
  filepath: "./journal/creator" 
  type: daily-log
  created: 2023-05-10 20:44:52 (1683776692.21)
  title: 2023-05-10 — 2023-05-10 Just some stuffs
---
# 2023-05-10 — 2023-05-10 Just some stuffs

Not really sure what's going on.  I should consider to do some form of looking at getting these from here...  To Ko-Fi.  

## Brief Update

A number of big projects going on.  

* Getting LedgerSMB properly setup
* Getting Mailman setup
* Gphoto2 is not working
* Big photo import and creating 

## Learning

* Gone back to DuoLingo 
* Been trying to figure some stuff out with Docker and seeing if that can be helpful with doing some diagnosis for LedgerSMB.

## Coding

Been working on some toolchain stuff for Twine games, and Git...  

## Writing

Mostly...  stuff with Ko-Fi but a couple of games working on...  The Wall of Aweful is kind of neat in that regard.  

## Painting/Drawing

Probably opened some stuff up...

## Sewing

Nothing really works...  

## Videos

More recording, no working on the recorded content...  

## Games 

Got some stuff for the switch.  Got Harvestella which is pretty cool.  

Want to give something of use to Rebecca in terms of having a bit of 10-60 minute "introduction" videos to some of the games I am playing...  

### Development

Didn't I have that up above?  Coding?  Development...  Need to think about those categories...  

### Playing

Been trying to get together with Rebbeca...  

## Daily Log

* -- New Notes Here --

Nothing really to say here.  Did some work on learning Docker.

## Format Info

[[Key]]

