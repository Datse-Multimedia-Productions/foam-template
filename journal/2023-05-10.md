---
foam-template: 
  filepath: "./journal/2023-05-10.md" 
  type: daily-note
  created: 2023-05-10 20:39:18 (1683776358.419)
  title: 2023-05-10 — 2023-05-10
---
# 2023-05-10 — 2023-05-10

See [[Bullet Journal Workflow]], [[Daily Note]], and [[2023-03-09 Testing Creators Journal]] for things that should be done here.

* [ ] Bullet Journal Note(s) Created
* [ ] Bullet Journal Note(s) Moved
* [x] Creators Journal(s) Created 
* [x] Creators Journal(s) Moved
* [ ] Daily Note Updated
* [ ] Migrate Bullet Journal

* Update of this template...  