# Inbox

- Here you can write disorganised notes to be categorised later
- Bullet points are useful, but it could be free form text as well
- Sometimes it's better to just get things off your mind quickly, rather than stop to think where it belongs
- But don't let this list get too long
- Move information to more specific documents and link to them.
  - This helps you navigate between documents quickly
  - For example, you can `Cmd`+`Click` (`Ctrl`+`Click` in Windows) this: [[todo]]
- Some notes don't end up making sense the next day
- That's ok, you can just delete them!
  - You can always find them in your git history, if you really need it!

Tasks...

Do something about Tasks.

stomatomenia datum seed Lif

seed - ? ? ? Datum

  mismotion elf Ata

  seed - ?? Elf Datum

  multistaminate some ease fed dod

  SEED - ? Elf Ease Datum


SEED - Sedum Elf Ease Datum 

OK, that'll work...  