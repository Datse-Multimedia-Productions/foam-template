---
foam-template:
  filepath: Bullet/Daily/2023-04-04-Updating Settings.md
  type: daily-log
  created: 2023-04-04 14:42:04 (1680644524.394)
  title: 2023-04-04 — Updating Settings
---
# 2023-04-04 — Updating Settings

## Daily Log

* [ ] {x} ~~Update settings to open new notes in current Directory~~
  * [ ] {x} ~~Does not work.~~

## Format Info

* basic note
* [ ] task note
* [x] completed task
* { } Indicators
  * {/} Partially done
  * {>} Moved to same level
  * {x} ~~Removed~~
  * {<} Moved up to higher level
* [ ] { } Task with Indicators

