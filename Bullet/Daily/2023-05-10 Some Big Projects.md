---
foam-template:
  name: Bullet Journal Daily Log
  description: Use for the Daily Log
  filepath: Bullet/Daily/2023-05-10-Some Big Projects.md
  type: daily-log
  created: 2023-05-10 20:39:33 (1683776373.668)
  title: 2023-05-10 — Some Big Projects
---
# 2023-05-10 — Some Big Projects

## Daily Log

* -- New Notes Here --
* [ ] {>} LedgerSMB Payments working
* [ ] {>} LedgerSMB Bill Shaw stuff
* [ ] {>} Mailman working
* [x] { } gphoto2 update
* [x] { } Photos Imported
* [x] { } Update Journal template
* [ ] {>} Look at writing stuff up in for scripts:
  * [ ] {>} VSCodium
    * [ ] {>} LaTeX
    * [ ] {>} Markdown
  * [ ] {>} vi
    * [ ] {>} LaTeX
    * [ ] {>} Markdown
  * [ ] {>} Format:
    * [ ] {>} LaTeX
    * [ ] {>} Markdown
  * [ ] {>} Scribus
    * [ ] {>} Scribus Format
* [ ] {>} Write Post about Journalling for Ko-Fi

## Key

* [[Key]]
