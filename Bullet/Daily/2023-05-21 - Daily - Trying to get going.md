---
foam-template:
  name: Bullet Journal Daily Log
  description: Use for the Daily Log
  filepath: Bullet/Daily/2023-05-21-2023-05-21 - Daily - Trying to get going.md
  type: daily-log
  created: 2023-05-21 15:07:43 (1684706863.129)
  title: 2023-05-21 — 2023-05-21 - Daily - Trying to get going
---
# 2023-05-21 — 2023-05-21 - Daily - Trying to get going

## Daily Log

* -- New Notes Here --
* Have some stuff in the Pantone 14-0760 Cyber Yellow Notebook
  * [ ] Foam Notes (that's this)
  * [ ] Linux Game Jam Planning
  * [ ] Adventure Game Jam Planning
  * [ ] Billing for Shaw Work
  * [ ] Mail Server (Gmail)
    * [ ] DMARC
    * [ ] SPF
    * [ ] DKIM
* Risto called to complain about not being able to send to our server...  He was not listening, and I got shirty with him, and "lost connection."  I *think* he hung up, but I'm not certain.  
* [x] Migrate
* [ ] { } LedgerSMB Payments working
* [ ] { } LedgerSMB Bill Shaw stuff
* [ ] { } Mailman working
* [ ] { } Look at writing stuff up in for scripts:
  * [ ] { } VSCodium
    * [ ] { } LaTeX
    * [ ] { } Markdown
  * [ ] { } vi
    * [ ] { } LaTeX
    * [ ] { } Markdown
  * [ ] { } Format:
    * [ ] { } LaTeX
    * [ ] { } Markdown
  * [ ] { } Scribus
    * [ ] { } Scribus Format
* [ ] { } Write Post about Journalling for Ko-Fi

## Key

* [[Key]]
