---
foam-template:
  filepath: Bullet/Daily/2023-03-24-Looking at Moddy Moon Audios.md
  type: daily-log
  created: 2023-03-24 19:53:56 (1679712836.521)
  title: 2023-03-24 — Looking at Moddy Moon Audios
---
# 2023-03-24 — Looking at Moddy Moon Audios

## Daily Log

* [x] Write Up Script List [[Script List]]
* [ ] {/} Write Induction Script
* [ ] {/} Write Introduction Script
* [x] Create Moddy Directory
* [x] Migrate from [[2023-02-28 Testing Daily Log]]

## Format Info

* basic note
* [ ] task note
* { } Indicators
* [ ] { } Task with Indicators

