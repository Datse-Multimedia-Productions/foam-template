---
foam-template:
  filepath: Bullet/Daily/2023-04-21-Getting Things Done.md
  type: daily-log
  created: 2023-04-21 15:37:08 (1682116628.577)
  title: 2023-04-21 — Getting Things Done
---
# 2023-04-21 — Getting Things Done

## Daily Log

* [ ] {>} Look at writing stuff up in for scripts:
  * [ ] { } VSCodium
    * [ ] { } LaTeX
    * [ ] { } Markdown
  * [ ] { } vi
    * [ ] { } LaTeX
    * [ ] { } Markdown
  * [ ] { } Format:
    * [ ] { } LaTeX
    * [ ] { } Markdown
  * [ ] { } Scribus
    * [ ] { } Scribus Format
* [ ] {<} Get Audio/Video tools together
  * [ ] { } Get Multi-track ffmpeg recording setup
  * [ ] { } Put h265 re-encode script somewhere easy to find
* [ ] { } Edit and post No Man's Sky video
* [ ] {<} Omutsu Cover pattern
  * [ ] { } Lining portion
  * [ ] { } "Wings" portion attachment
  * [ ] { } Snaps locations
  * [ ] { } Print and Test 
* [ ] {x} ~Diaper Trainer setup timers for:~
  * [ ] {x} ~Changes~
  * [ ] {x} ~Wettings~
  * [ ] {x} ~Messings~
* [x] { } Migrate Daily Log
  
## Format Info

* basic note
* [ ] task note
* [x] completed task
* { } Indicators
  * {/} Partially done
  * {>} Moved to same level
  * {x} ~~Removed~~
  * {<} Moved up to higher level
* [ ] { } Task with Indicators

