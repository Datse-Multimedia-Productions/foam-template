---
foam-template:
  name: Bullet Journal Daily Log
  description: Use for the Daily Log
  filepath: Bullet/Daily/2023-04-22-Saturday Working.md
  type: daily-log
  created: 2023-04-22 16:34:19 (1682206459.781)
  title: 2023-04-22 — Saturday Working
---
# 2023-04-22 — Saturday Working

## Daily Log

* -- New Notes Here --
* [x] Migrate
* [x] Rename/Move
* [ ] {>} Look at writing stuff up in for scripts:
  * [ ] {>} VSCodium
    * [ ] {>} LaTeX
    * [ ] {>} Markdown
  * [ ] {>} vi
    * [ ] {>} LaTeX
    * [ ] {>} Markdown
  * [ ] {>} Format:
    * [ ] {>} LaTeX
    * [ ] {>} Markdown
  * [ ] {>} Scribus
    * [ ] {>} Scribus Format
* [ ] { } Write Post about Journalling for Ko-Fi

## Key

* [[Key]]
