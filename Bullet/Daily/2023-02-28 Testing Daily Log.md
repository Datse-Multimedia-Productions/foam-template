---
type: daily-log
created: 2023-02-28 16:11:50 (1677629510.79)
title: 2023-02-28 — 2023-02-28 Testing Daily Log
---
# 2023-02-28 — 2023-02-28 Testing Daily Log

## Daily Log

* [x] This is just testing the daily Log 
* [x] That is a task as is this.
* This is not a task.
* { } This is a note with Indicators (none set)
* [x] { } This is a task with Indicators.
* [x] Goes back to a task.

* [[Bullet Journal Workflow]] For Reference

## Format Info

* basic note
* [ ] task note
* { } Indicators
* [ ] { } Task with Indicators

