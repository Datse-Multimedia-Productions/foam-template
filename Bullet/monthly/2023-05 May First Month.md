---
foam-template:
  filepath: Bullet/Weekly/2023-05-10-2023-05 May First Month.md
  type: monthly-log
  created: 2023-05-10 20:54:04 (1683777244.475)
  title: 2023-05-10 — 2023-05 May First Month
---
# 2023-05 — 2023-05 May First Month — 2023-05-10+7d

## 05

* 1
  * [ ] Task
  * Event
  * [ ] {BD} Stephen's #Birthday
* 2
  * [ ] Task
  * Event
* 3
  * [ ] Task
  * Event
* 4
  * [ ] Task
  * Event
* 5
  * [ ] Task
  * Event
* 6
  * [ ] Task
  * Event
* 7
  * [ ] Task
  * Event
* 8
  * [ ] Task
  * Event
* 9
  * [ ] Task
  * Event
  * [ ] Migrate
* 10
  * [ ] Task
  * Event
* 11
  * [ ] Task
  * Event
* 12
  * [ ] Task
  * Event
* 13
  * [ ] Task
  * Event
* 14
  * [ ] Task
  * Event
* 15
  * [ ] Task
  * Event
* 16
  * [ ] Task
  * Event
* 17
  * [ ] Task
  * Event
* 18
  * [ ] Task
  * Event
* 19
  * [ ] Task
  * Event
* 20
  * [ ] Task
  * Event
* 21
  * [ ] Task
  * Event
* 22
  * [ ] Task
  * Event
* 23
  * [ ] Task
  * Event
* 24
  * [ ] Task
  * Event
* 25
  * [ ] Task
  * Event
* 26
  * [ ] Task
  * Event
* 27
  * [ ] Task
  * Event
* 28
  * [ ] Task
  * Event
  * [ ] Monthly Billing
* 29
  * [ ] Task
  * Event
* 30
  * [ ] Task
  * Event
* 31
  * [ ] Task
  * Event

### Monthly Notes

* New Notes Here:

## Key

* [[Key]]