---
foam-template:
  filepath: Bullet/Future/2023-04-21-2023-04 Future Log.md
  type: monthly-log
  created: 2023-04-21 21:16:52 (1682137012.595)
  title: 2023-04-21 — 2023-04 Future Log
---
# 2023-04 — 2023-04 Future Log — 2023-04+1y

## 2023

### 04 2023 April

### 05 2023 May

* [ ] {BD, >} Stephen's #Birthday

### 06 2023 June

### 07 2023 July

### 08 2023 August

### 09 2023 September

### 10 2023 October

### 11 2023 November

### 12 2023 December

* [ ] Migrate Future Log

### 13 2024 January

### 14 2024 February

### 15 2024 March

### 16 2024 April

### Future Notes

* New Notes Here:

## Key

* [[Key]]
