---
foam-template:
  filepath: Bullet/Weekly/2023-04-21-Mostly Testing Weekly Log.md
  type: weekly-log
  created: 2023-04-21 16:51:41 (1682121101.712)
  title: 2023-04-21 — Mostly Testing Weekly Log
---
# 2023-04-21 — Mostly Testing Weekly Log — 2023-04-28

Update 

## Weekly Log

* New Notes Here and VVV there 

### Friday

* Started Weekly Log

### Saturday

### Sunday

### Monday

### Tuesday

### Wednesday

### Thursday

* [ ] Migrate Weekly Log
* [ ] Create Montly Log for May

### Friday

### Notes and Goals

* Create video comparing different ways to write a script [[2023-04-21 Getting Things Done]]
* [ ] { } Get Audio/Video tools together
  * [ ] { } Get Multi-track ffmpeg recording setup
  * [ ] { } Put h265 re-encode script somewhere easy to find
* [ ] { } Omutsu Cover pattern
  * [ ] { } Lining portion
  * [ ] { } "Wings" portion attachment
  * [ ] { } Snaps locations
  * [ ] { } Print and Test 


## Key

* [[Key]]

