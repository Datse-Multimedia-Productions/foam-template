---
type: basic-note
created: 2023-04-21 16:37:19 (1682120239.56)
title: key
---
# Bullet Journal Key

This is an evolving documentation of the way we are keeping things tracked here.  This applies to the [[Bullet Journal Workflow]], and [[Creator Journal]].

## Format Info

* basic note
* [ ] task note
* [x] completed task

## Signifiers

This is based on the idea which is from the Bullet Journal.  This likely will be added to along the way as things get modified.  

* { } Signifiers
  * {/} Partially done
  * {>} Moved to same level
  * {x} ~~Removed~~
  * {<} Moved up to higher level
  * {BD} #Birthday
  * {E} Event
  * {*} Important
  * {!} Inspiration
  * {EXAM} Examine Further
* [ ] { } Task with Indicators
