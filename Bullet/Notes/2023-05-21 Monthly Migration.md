---
type: basic-note
created: 2023-05-21 15:24:10 (1684707850.246)
title: 2023-05-21 Monthly Migration
---
# 2023-05-21 Monthly Migration

Today I changed the Monthly log template file, so that it only has the dates, and no notes about the dates.  That makes more sense to me.  This *probably* will need to be updated along the way as I do more about this.  