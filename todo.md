# To Do

Documentation is at [[To Do Workflow]].

## 2023 February 21

- [ ] Import from Dendron
  - [ ] Ko-Fi 
  - [ ] Moddy Moon
  - [ ] Creative
  - [ ] Dom Nelson
- [ ] Create Document for Modem settings
- [x] Update PeerTube

## 2023 February 28

- [ ] Create Daily Tasks
- [ ] Create Weekly Tasks
- [ ] Create Monthly Tasks
- [ ] Note about opening preview
  - [ ] Need to open from markdown file