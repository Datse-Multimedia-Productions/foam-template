# Git

VSCodium does not properly handle Git in a way that works for me in terms of how it handles the committing et al.  I have to do those commands that push and pull from Git through the terminal.

## Setting up Authentication

```
$ eval $(ssh-agent -s)
$ ssh-add [key-to-add]
```

## Adding Files

```
$ git add [file]
```

This will add the file, it is entered for the next commit.

## Commit

```
$ git commit
```

This will ask for a commit message.  Try to keep them useful so that it's clear what is getting committed.

## Pushing

```
$ git push
```

If this works then you can update the remote repository.

## Status

It is sometimes useful to get the status, with:

```
$ git status
```

It can help to know that what is getting pushed or not getting pushed is useful.  