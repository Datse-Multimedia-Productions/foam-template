---
type: basic-note
created: 2023-04-21 21:24:44 (1682137484.255)
title: Creator Journal
---
# Creator Journal

This is the journal we keep with regards to the creative stuff we are doing.  It's somewhat involved, but anything can be skipped, or just a very brief note made for it.