---
Title: Graph Visualization
---

# Graph Visualisation

This is a feature that shows what the graph of yoru documetation looks like.

It can be accessed through:

The command `Foam: Show Graph` which can be accessed with `ctrl-shift-p` and typing in the command.  

This can be useful to find notes that are not connected to the main graph.  In writing this up, I saw that I could *not* see the "your-first-template.md" being connected to the graph, and it was just sitting there all by its lonesome (which in a lot of ways is exactly what this specific one would be expected to do).  But it was also useful in that it lead to the new feature I hadn't seen how it works.  