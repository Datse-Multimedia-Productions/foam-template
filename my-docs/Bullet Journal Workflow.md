---
type: basic-note
created: 2023-02-28 16:07:42 (1677629262.906)
title: Bullet Journal Workflow
---
# Bullet Journal Workflow

This should change...  But for now this is it:

`ctrl-shift-p` to open the command pallet:

`Foam: New Note from Template` - To Create New Note

Select Template:

* bullet-daily-log — As of now, only one.
* bullet-weekly-log — Should be available Soon.
* bullet-monthly-log — That should be shortly after.
* bullet-future-log — Full year, will likely take some time.

Move note into appropriate section in the Bullet section.