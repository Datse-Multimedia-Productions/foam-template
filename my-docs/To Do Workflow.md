---
type: basic-note
created: 2023-02-28 15:55:07 (1677628507.16)
title: To Do Workflow
---
# To Do Workflow

Creating To Do Lists:

```
* [ ] To Do Item
  * [ ] Nested To Do Item
* [x] Completed To Do Item
```

* [ ] To Do Item
  * [ ] Nested To Do Item
* [x] Completed To Do Item

Items can be toggled with `alt-c`. 

