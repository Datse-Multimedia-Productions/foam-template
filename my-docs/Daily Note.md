# Daily Note

The Daily Note function (specifically `Foam: Open Daily Note`) is the built in journal function for Foam.  It will let you open a daily note, from any day.  It defaults to the current day.  It can be opened with teh [[Command Pallet]] (It think that's the correct term), `ctrl-shift-p` and searching for it.  It also has the `alt-h` shortcut assigned to it for me.


