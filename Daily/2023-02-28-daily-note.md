# 2023-02-28-daily-note.md

OK, this will open (with keybinding set as) with `ctrl-alt-d`.  But while it opens "where defined" (which isn't where I want it), I've not got the note template to work for it.  

I'll work on that.