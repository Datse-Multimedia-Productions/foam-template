---
foam-template: 
  filepath: "./journal/creator" 
  type: daily-log
  created: ${FOAM_DATE_YEAR}-${FOAM_DATE_MONTH}-${FOAM_DATE_DATE} ${FOAM_DATE_HOUR}:${FOAM_DATE_MINUTE}:${FOAM_DATE_SECOND} (${FOAM_DATE_SECONDS_UNIX})
  title: ${FOAM_DATE_YEAR}-${FOAM_DATE_MONTH}-${FOAM_DATE_DATE} — ${FOAM_TITLE}
---
# ${FOAM_DATE_YEAR}-${FOAM_DATE_MONTH}-${FOAM_DATE_DATE} — ${FOAM_TITLE}

## Brief Update

## Learning

## Coding

## Writing

## Painting/Drawing

## Sewing

## Videos

## Games 

### Development

### Playing

## Daily Log

* -- New Notes Here --

## Format Info

[[Key]]

