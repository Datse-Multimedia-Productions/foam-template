---
foam-template:
  filepath: Bullet/Weekly/${FOAM_DATE_YEAR}-${FOAM_DATE_MONTH}-${FOAM_DATE_DATE}-${FOAM_TITLE}.md
  type: monthly-log
  created: ${FOAM_DATE_YEAR}-${FOAM_DATE_MONTH}-${FOAM_DATE_DATE} ${FOAM_DATE_HOUR}:${FOAM_DATE_MINUTE}:${FOAM_DATE_SECOND} (${FOAM_DATE_SECONDS_UNIX})
  title: ${FOAM_DATE_YEAR}-${FOAM_DATE_MONTH}-${FOAM_DATE_DATE} — ${FOAM_TITLE}
---
# ${FOAM_DATE_YEAR}-${FOAM_DATE_MONTH} — ${FOAM_TITLE} — ${FOAM_DATE_YEAR}-${FOAM_DATE_MONTH}-${FOAM_DATE_DATE}+7d

## ${FOAM_DATE_MONTH}

* 1
* 2
* 3
* 4
* 5
* 6
* 7
* 8
* 9
* 10
* 11
* 12
* 13
* 14
* 15
* 16
* 17
* 18
* 19
* 20
* 21
* 22
* 23
* 24
* 25
* 26
* 27
* 28
  * [ ] Monthly Migration
* 29
* 30
* 31

### Monthly Notes

* New Notes Here:

## Key

* [[Key]]
* [[2023-05-21 Monthly Migration]]