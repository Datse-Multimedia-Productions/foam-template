---
foam-template: 
  filepath: "./journal/$FOAM_DATE_YEAR-$FOAM_DATE_MONTH-$FOAM_DATE_DATE.md" 
  type: daily-note
  created: ${FOAM_DATE_YEAR}-${FOAM_DATE_MONTH}-${FOAM_DATE_DATE} ${FOAM_DATE_HOUR}:${FOAM_DATE_MINUTE}:${FOAM_DATE_SECOND} (${FOAM_DATE_SECONDS_UNIX})
---
# ${FOAM_DATE_YEAR}-${FOAM_DATE_MONTH}-${FOAM_DATE_DATE} — ${FOAM_TITLE}

See [[Bullet Journal Workflow]], [[Daily Note]], and [[2023-03-09 Testing Creators Journal]] for things that should be done here.

* [ ] Bullet Journal Note(s) Created
* [ ] Bullet Journal Note(s) Moved
* [ ] Bullet Journal Migration(s)
* [ ] Creators Journal(s) Created
* [ ] Creators Journal(s) Moved
* [ ] Daily Note Updated
* [ ] Migrate Bullet Journal
