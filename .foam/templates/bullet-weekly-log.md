---
foam-template:
  filepath: Bullet/Weekly/${FOAM_DATE_YEAR}-${FOAM_DATE_MONTH}-${FOAM_DATE_DATE}-${FOAM_TITLE}.md
  type: weekly-log
  created: ${FOAM_DATE_YEAR}-${FOAM_DATE_MONTH}-${FOAM_DATE_DATE} ${FOAM_DATE_HOUR}:${FOAM_DATE_MINUTE}:${FOAM_DATE_SECOND} (${FOAM_DATE_SECONDS_UNIX})
  title: ${FOAM_DATE_YEAR}-${FOAM_DATE_MONTH}-${FOAM_DATE_DATE} — ${FOAM_TITLE}
---
# ${FOAM_DATE_YEAR}-${FOAM_DATE_MONTH}-${FOAM_DATE_DATE} — ${FOAM_TITLE} — ${FOAM_DATE_YEAR}-${FOAM_DATE_MONTH}-${FOAM_DATE_DATE}+7d

Update 

## Weekly Log

* New Notes Here and VVV there 

### ${CURRENT_DAY_NAME}

### ${CURRENT_DAY_NAME}+1

### ${CURRENT_DAY_NAME}+2

### ${CURRENT_DAY_NAME}+3

### ${CURRENT_DAY_NAME}+4

### ${CURRENT_DAY_NAME}+5

### ${CURRENT_DAY_NAME}+6

### ${CURRENT_DAY_NAME}+7

### Notes and Goals

## Key

* [[Key]]

