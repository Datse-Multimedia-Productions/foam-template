---
foam-template: 
  filepath: "./mydocs/${FOAM_TITLE}.md" 
  type: documentation
  created: ${FOAM_DATE_YEAR}-${FOAM_DATE_MONTH}-${FOAM_DATE_DATE} ${FOAM_DATE_HOUR}:${FOAM_DATE_MINUTE}:${FOAM_DATE_SECOND} (${FOAM_DATE_SECONDS_UNIX})
  title: ${FOAM_DATE_YEAR}-${FOAM_DATE_MONTH}-${FOAM_DATE_DATE} — ${FOAM_TITLE}
  tags: documentation
---
# ${FOAM_TITLE}