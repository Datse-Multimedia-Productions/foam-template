---
type: character
created: ${FOAM_DATE_YEAR}-${FOAM_DATE_MONTH}-${FOAM_DATE_DATE} ${FOAM_DATE_HOUR}:${FOAM_DATE_MINUTE}:${FOAM_DATE_SECOND} (${FOAM_DATE_SECONDS_UNIX})
tags: character
---
# ${FOAM_TITLE}

## Basics

* Ethnicity
* Gender
* Age
* Time
* Occupation
* Attitude

## Ethnicity Research

## Gender Research

## Age Research

## Time Research

## Occupation Research

## Attitude Research

## Bringing it All Together
