<img src="attachments/foam-icon.png" width=100 align="left">

# Foam

**👋 Welcome to your new Foam Knowledge Base!**

## Getting started

This is completed so I'm dropping it.  

## Using Foam

We've created a few Bubbles (Markdown documents) to get you started.

- [inbox](./inbox.md) - a place to write down quick notes to be categorized later
- [getting-started](./getting-started.md) - learn how to use your Foam workspace
- [todo](./todo.md) - a place to keep track of things to do

In the `docs` directory you can find everything you need to learn the basics of Foam.

## Submitting Issues

As you may have noticed, issues are disabled. With the goal to keep the project fairly easy to maintain, please file your issues in the main Foam repository:

> <https://github.com/foambubble/foam>
